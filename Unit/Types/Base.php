<?php

/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 0:37
 */

namespace Game\Unit\Types;

abstract class Base
{

    private $positionX = 0;
    private $positionY = 0;
    protected $playerId;
    protected $health;

    public function __construct(int $x, int $y)
    {
        $this->positionX = $x;
        $this->positionY = $y;
    }

    /**
     * @param mixed $health
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }

    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param mixed $playerId
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
    }

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param int $positionX
     */
    public function setPositionX($positionX)
    {
        $this->positionX = $positionX;
    }

    /**
     * @return int
     */
    public function getPositionX()
    {
        return $this->positionX;
    }

    /**
     * @param int $positionY
     */
    public function setPositionY($positionY)
    {
        $this->positionY = $positionY;
    }

    /**
     * @return int
     */
    public function getPositionY()
    {
        return $this->positionY;
    }

    /**
     * @return array
     */
    public static function getAcceptableTerrain():array
    {
        return static::$acceptableTerrain;
    }

    public static function getAcceptableAttacking(): array
    {
        return static::$acceptableAttacking;
    }

}