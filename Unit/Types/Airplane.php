<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 21:37
 */

namespace Game\Unit\Types;


use Game\Terrain\Types\Land;
use Game\Terrain\Types\Mountains;
use Game\Terrain\Types\Swamp;
use Game\Terrain\Types\Water;
use Game\Unit\Actions\IAttacking;
use Game\Unit\Actions\IMovable;
use Game\Unit\Actions\TAttacking;
use Game\Unit\Actions\TMovable;

class Airplane extends Base implements IMovable, IAttacking
{
    use TMovable, TAttacking;

    protected $health = 150;
    protected $attackingDamage = 35;

    protected static $acceptableTerrain = [
        Land::class,
        Mountains::class,
        Water::class,
        Swamp::class
    ];

    protected static $acceptableAttacking = [
        Vehicle::class
    ];
}