<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 22:23
 */

namespace Game\Unit\Actions;


use Game\Unit\Exception\CantAttack;
use Game\Unit\Types\Base;

trait TAttacking
{

    /**
     * Attacking method
     * @param Base $unit
     * @throws CantAttack
     */
    public function attack(Base $unit)
    {
        if(in_array(get_class($unit), self::getAcceptableTerrain())){
            throw new CantAttack('Can\'t attack this type of unit');
        }

        if ($unit->getPlayerId() == $this->getPlayerId()){
            throw new CantAttack('Can\'t attack unit of the same player');
        }

        $unit->setHealth($unit->getHealth() - $this->getAttackingDamage());
    }

    /**
     * Get attacking damage
     * @return mixed
     */
    public function getAttackingDamage()
    {
        return $this->attackingDamage;
    }

}