<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 0:07
 */

namespace Game;

use Game\Render\Colors;
use Game\Render\Render;
use Game\Unit\Types\Airplane;
use Game\Unit\Types\Base;
use Game\Unit\Types\Camp;
use Game\Unit\Types\Human;
use Game\Unit\Types\Vehicle;

class GameSandbox{

    private $gameInstance;

    public function __construct(Game $game)
    {
        $this->gameInstance = $game;
        return $this;
    }

    public function generateTerrain()
    {
        $this->gameInstance->createTerrain();
        return $this;
    }

    public function generatePlayers(int $playersCount = 2)
    {
        for($count = 0; $count < $playersCount; $count++){
            $this->gameInstance->createPlayer();
        }

        return $this;
    }

    public function generateUnits()
    {
        $mapSize = $this->gameInstance->getMapsize();

        $campTypes = Camp::getAcceptableTerrain();
        $humanTypes = Human::getAcceptableTerrain();
        $vehicleTypes = Vehicle::getAcceptableTerrain();
        $airplaneTypes = Airplane::getAcceptableTerrain();

        $humanCount = floor(sqrt($mapSize));
        $airplaneCount = $vehicleCount = floor(sqrt($mapSize)/2);

        foreach ($this->gameInstance->getPlayers() as $player) {
            //create camps
            $this->addUnit($campTypes, $player, Camp::class);
            //create humans
            for($subi = 0; $subi < $humanCount; $subi++){
                $this->addUnit($humanTypes, $player, Human::class);
            }
            //create vehicle
            for($subi = 0; $subi < $vehicleCount; $subi++){
                $this->addUnit($vehicleTypes, $player, Vehicle::class);
            }
            //create airplanes
            for($subi = 0; $subi < $airplaneCount; $subi++){
                $this->addUnit($airplaneTypes, $player, Airplane::class);
            }
        }

        return $this;
    }

    public function render()
    {
        $render = new Render($this->gameInstance, new Colors());
    }
    

    private function addUnit($terrainTypes, $player, $unitType)
    {
        $terCells = $this->gameInstance->getAcceptableTerrainFreeCells($terrainTypes);
        $cell = $terCells[array_rand($terCells)];
        $this->gameInstance->createUnit($player, $unitType, $cell['x'], $cell['y']);
    }



}