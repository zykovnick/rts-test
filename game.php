<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 07.05.17
 * Time: 17:44
 */

namespace Game;

spl_autoload_register(function($class) {
    if (stripos($class, __NAMESPACE__) === 0)
    {
        include(__DIR__ . str_replace('\\', DIRECTORY_SEPARATOR, substr($class, strlen(__NAMESPACE__))) . '.php');
    }
}
);

$params = [
    ''          => 'help',
    'ms::'      => 'mapsize::',
    'wd::'      => 'wden::',
    'sd::'      => 'sden::',
    'md::'      => 'mden::',
];
$options = getopt( implode('', array_keys($params)), $params );

$mapsize = 16;
$wDen = 2;
$mDen = 2;
$sDen = 2;

if (isset($options['mapsize']) || isset($options['mapsize']))
{
    $mapsize = isset( $options['mapsize'] ) ? $options['mapsize'] : $options['ms'];
}

if (isset($options['wden']) || isset($options['wden']))
{
    $wDen = isset( $options['wden'] ) ? $options['wden'] : $options['wd'];
}

if (isset($options['mden']) || isset($options['mden']))
{
    $mDen = isset( $options['mden'] ) ? $options['mden'] : $options['md'];
}

if (isset($options['sden']) || isset($options['sden']))
{
    $sDen = isset( $options['sden'] ) ? $options['sden'] : $options['sd'];
}

if($mapsize < 2){
    $mapsize = 2;
}

try {
    $gameSandbox = new GameSandbox(
        new Game($mapsize, $wDen, $mDen, $sDen)
    );

    $gameSandbox->generateTerrain()
        ->generatePlayers()
        ->generateUnits()
        ->render();

}catch (\Game\Exception\Base $e){

}