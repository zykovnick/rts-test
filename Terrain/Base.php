<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 07.05.17
 * Time: 20:02
 */


namespace Game\Terrain;


use Game\Terrain\Types\IBase;
use Game\Terrain\Types\Land;

class Base{

    /**
     * Array of terrain objects
     * @var array
     */
    protected $terrain = [];
    /**
     * Size of map
     * @var int
     */
    protected $mapSize;

    /**
     * Base constructor.
     * Create terrain and fill it with land
     * @param int $size
     */
    public function __construct(int $size)
    {
        $this->mapSize = $size;
        for ($width = 0; $width < $this->mapSize; $width++){
            for ($height = 0; $height < $size; $height++) {
                $this->setCell($width, $height, new Land());
            }
        }

        return $this;
    }

    /**
     * Add custom terrain type to map
     * @param IBase $type
     * @param int $coef
     * @return Base
     */
    public function addTerrainType(IBase $type, int $coef = 2): Base
    {
        $waterCellCount = $this->calculateCells($coef);
        $landCells = $this->getCellsByType(new Land());
        $landCellsCount = count($landCells);

        for ($count = 0; $count < $waterCellCount; $count++){
            $cellRand = rand(0, $landCellsCount-1);
            $this->setCell(
                $landCells[$cellRand]['x'],
                $landCells[$cellRand]['y'],
                new $type
            );

        }

        return $this;
    }

    /**
     * Calculate count of cell to set by coefficient
     * @param int $coef
     * @return int
     */
    private function calculateCells(int $coef): int
    {
        $cellCount = floor(pow($this->mapSize, 2)*$coef/10);
        return $cellCount;
    }

    /**
     * Setting cell
     * @param int $x
     * @param int $y
     * @param IBase $cell
     * @return bool
     */
    public function setCell(int $x, int $y, IBase $cell): bool
    {
        $this->terrain[$x][$y] = $cell;
        return true;
    }

    /**
     * Return cell object
     * @param int $x
     * @param int $y
     * @return IBase
     */
    public function getCell(int $x, int $y): IBase
    {
        return $this->terrain[$x][$y];
    }

    /**
     * Get cells by custom terrain type
     * @param IBase $type
     * @return array
     */
    public function getCellsByType(IBase $type): array
    {
        $cells = [];

        for ($width = 0; $width < $this->mapSize; $width++){
            for ($height = 0; $height < $this->mapSize; $height++) {
                if($this->getCell($width, $height) instanceof $type){
                    $cells[] = [
                        'x' =>  $width,
                        'y' =>  $height
                    ];
                }
            }
        }

        return $cells;
    }

    /**
     * @return array
     */
    public function getTerrainArray(): array
    {
        return $this->terrain;
    }

}