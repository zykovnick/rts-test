<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 07.05.17
 * Time: 18:41
 */

namespace Game\Player;

use Game\Unit\UnitFactory;

class Player{

    /**
     * @var int
     */
    private $playerId;

    private $unitFactory;

    /**
     * Player constructor.
     * @param int $playerId
     */
    public function __construct(int $playerId)
    {
        $this->playerId = $playerId;
        $this->unitFactory = new UnitFactory($this);
    }

    /**
     * @return int
     */
    public function getPlayerId(): int
    {
        return $this->playerId;
    }

    /**
     *
     * @param $unitType
     * @param int $x
     * @param int $y
     * @return \Game\Unit\Types\Base
     */
    public function createUnit($unitType, int $x, int $y)
    {
        return $this->unitFactory->createUnit($unitType, $x, $y);
    }

}