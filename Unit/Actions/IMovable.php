<?php

/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 0:42
 */

namespace Game\Unit\Actions;

use Game\Terrain\Types\IBase;

interface IMovable
{
    public function moveTo(IBase $cell, $x, $y);
}