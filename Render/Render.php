<?php

/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 22:49
 */

namespace Game\Render;

use Game\Game;

class Render
{

    private $gameInstance;
    private $map;
    private $colorString;
    private $playersColors = [];

    public function __construct(Game $game, Colors $colorString)
    {
        $this->gameInstance = $game;
        $this->colorString = $colorString;
        $this->buildMap();
        $this->setPlayersColor();
        $this->renderMap();
    }

    /**
     * build map matrix
     */
    private function buildMap(){
        $units = $this->gameInstance->getUnits();
        $this->map = $this->gameInstance->getTerrain()->getTerrainArray();
        foreach ($units as $unit){
            $this->map[$unit->getPositionX()][$unit->getPositionY()] = $unit;
        }
    }

    /**
     * get random color
     * @return mixed
     */
    private function generatePlayerColor()
    {
        $unique = true;
        $generatedColor = array_rand($this->colorString->getPlayersColors());
        foreach ($this->playersColors as $color){
            if($color == $generatedColor){
                $unique = false;
            }
        }

        if ($unique){
            return $generatedColor;
        }else{
            return $this->generatePlayerColor();
        }
    }

    /**
     * setting color to each player
     */
    private function setPlayersColor(){
        foreach ($this->gameInstance->getPlayers() as $player){
            $this->playersColors[$player->getPlayerId()] = $this->generatePlayerColor();
        }
    }

    /**
     * render map
     */
    private function renderMap()
    {
        foreach ($this->map as $row){
            foreach ($row as $cell){
               echo $this->renderCell($cell);
            }
            echo "\n";
        }
    }

    /**
     * render single cell
     * @param $cell
     * @return string
     */
    private function renderCell($cell){
        switch (get_class($cell)){
            case "Game\\Terrain\\Types\\Mountains":
                $cellString = $this->colorString->getColoredString('M', 'black', 'green');
                    break;

            case "Game\\Terrain\\Types\\Land":
                $cellString = $this->colorString->getColoredString(' ', 'green', 'green');
                break;

            case "Game\\Terrain\\Types\\Swamp":
                $cellString = $this->colorString->getColoredString(' ', 'yellow', 'yellow');
                break;

            case "Game\\Terrain\\Types\\Water":
                $cellString = $this->colorString->getColoredString(' ', 'blue', 'blue');
                break;

            case "Game\\Unit\\Types\\Camp":
                $cellString = $this->colorString->getColoredString('C', 'white', $this->getPlayerColor($cell));
                break;

            case "Game\\Unit\\Types\\Human":
                $cellString = $this->colorString->getColoredString('H', 'white', $this->getPlayerColor($cell));
                break;

            case "Game\\Unit\\Types\\Vehicle":
                $cellString = $this->colorString->getColoredString('V', 'white', $this->getPlayerColor($cell));
                break;

            case "Game\\Unit\\Types\\Airplane":
                $cellString = $this->colorString->getColoredString('A', 'white', $this->getPlayerColor($cell));
                break;


            default:
                $cellString = $this->colorString->getColoredString('X', 'white', 'red');
                break;

        }

        return $cellString;

    }

    /**
     * @todo check is it unit in cell or just land
     * get player's color by unit in cell
     * @param $cell
     * @return mixed
     */
    private function getPlayerColor($cell){
        return $this->colorString->getPlayersColors()[$this->playersColors[$cell->getPlayerId()]];
    }

}