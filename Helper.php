<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 19:18
 */

namespace Game;

class Helper{

    /**
     * Return serialized arrays (for compare in multidim arrays)
     * @param $arr
     * @return array
     */
    public static function serializeArrayValues($arr): array
    {
        foreach($arr as $key=>$val){
            $arr[$key] = serialize($val);
        }
        return $arr;
    }

}