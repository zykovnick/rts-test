<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 22:21
 */



namespace Game\Unit\Actions;

use Game\Unit\Types\Base;

interface IAttacking
{
    public function attack(Base $unit);

    public function getAttackingDamage();

}