<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 20:43
 */

namespace Game\Unit\Actions;

use Game\Terrain\Types\IBase;
use Game\Unit\Exception\CantMove;

trait TMovable
{
    /**
     * @todo refactor ibase to keep x and y in it
     * @param IBase $cell
     * @param $x
     * @param $y
     * @throws CantMove
     */
    public function moveTo(IBase $cell, $x, $y)
    {
        if(in_array(get_class($cell), self::getAcceptableTerrain())){
            throw new CantMove('Can\'t move in this cell');
        }

        $this->setPositionX($x);
        $this->setPositionY($y);
    }

}