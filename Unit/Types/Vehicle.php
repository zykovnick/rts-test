<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 10.05.17
 * Time: 20:42
 */

namespace Game\Unit\Types;

use Game\Terrain\Types\Land;
use Game\Terrain\Types\Swamp;
use Game\Unit\Actions\IAttacking;
use Game\Unit\Actions\TAttacking;
use Game\Unit\Actions\TMovable;
use Game\Unit\Actions\IMovable;

class Vehicle extends Base implements IMovable, IAttacking
{
    use TMovable, TAttacking;

    protected $health = 250;
    protected $attackingDamage = 100;

    protected static $acceptableTerrain = [
        Land::class,
        Swamp::class
    ];

    protected static $acceptableAttacking = [
        Human::class,
        Vehicle::class
    ];

}