<?php

/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 0:38
 */
namespace Game\Unit\Types;


use Game\Terrain\Types\Land;
use Game\Terrain\Types\Mountains;
use Game\Terrain\Types\Water;
use Game\Unit\Actions\IAttacking;
use Game\Unit\Actions\TAttacking;
use Game\Unit\Actions\TMovable;
use Game\Unit\Actions\IMovable;

class Human extends Base implements IMovable, IAttacking
{
    use TMovable, TAttacking;

    protected $health = 125;
    protected $attackingDamage = 25;

    protected static $acceptableTerrain = [
        Land::class,
        Mountains::class,
        Water::class
    ];

    protected static $acceptableAttacking = [
        Human::class,
        Vehicle::class
    ];
}