<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 0:05
 */

namespace Game;

use Game\Player\Player;
use Game\Terrain\Base;
use Game\Terrain\Types\Mountains;
use Game\Terrain\Types\Swamp;
use Game\Terrain\Types\Water;


class Game{

    protected $mapSize;
    protected $terrain;
    protected $players = [];
    protected $units = [];

    protected $waterDensity;
    protected $mountainsDensity;
    protected $swampDensity;

    /**
     * Game constructor.
     * @param int $mapSize
     * @param int $waterDensity
     * @param int $mountainsDensity
     * @param int $swampDensity
     */
    public function __construct(int $mapSize, int $waterDensity, int $mountainsDensity, int $swampDensity)
    {
        $this->mapSize = $mapSize;
        $this->waterDensity = $waterDensity;
        $this->mountainsDensity = $mountainsDensity;
        $this->swampDensity = $swampDensity;
    }

    /**
     * @return int
     */
    public function getMapsize():int
    {
        return $this->mapSize;
    }

    /**
     * @return array
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    /**
     * @return mixed
     */
    public function getTerrain()
    {
        return $this->terrain;
    }

    /**
     * generating terrain
     * @return bool
     */
    public function createTerrain(): bool
    {
        $terrain = new Base($this->mapSize);
        $terrain->addTerrainType(new Water(), $this->waterDensity)
            ->addTerrainType(new Mountains(), $this->mountainsDensity)
            ->addTerrainType(new Swamp(), $this->swampDensity);

        $this->terrain = $terrain;
        return true;
    }

    /**
     * Create new player
     * @return bool
     */
    public function createPlayer():bool
    {
        $this->players[] = new Player(count($this->players)+1);
        return true;
    }

    /**
     * @return array
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * Create unit using player unit factoey
     * @param Player $player
     * @param $unitType
     * @param int $x
     * @param int $y
     */
    public function createUnit(Player $player, $unitType, int $x, int $y)
    {
        $this->units[] = $player->createUnit($unitType, $x, $y);
    }

    /**
     * Get acceptable terrain cells
     * @param $terrainTypes
     * @return array
     */
    public function getAcceptableTerrainFreeCells($terrainTypes): array
    {
        $terrainAcceptableCells = [];
        $units = $this->getUnits();

        foreach ($terrainTypes as $terrainType) {
            $terrainAcceptableCells = array_merge($terrainAcceptableCells, $this->getTerrain()->getCellsByType(new $terrainType));
        }

        if(!empty($units)) {
            $unitCells = [];

            foreach ($units as $unit) {
                $unitCells[] = [
                    'x' =>  $unit->getPositionX(),
                    'y' =>  $unit->getPositionY()
                ];
            }

            $terrainAcceptableCells = array_map("unserialize", array_diff(
                    Helper::serializeArrayValues($terrainAcceptableCells),
                    Helper::serializeArrayValues($unitCells)
                )
            );

        }

        return $terrainAcceptableCells;
    }


}