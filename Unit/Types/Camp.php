<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 08.05.17
 * Time: 17:54
 */

namespace Game\Unit\Types;


use Game\Terrain\Types\Land;

class Camp extends Base
{
    protected $health = 300;

    protected static $acceptableTerrain = [
        Land::class
    ];
}