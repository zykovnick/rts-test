<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 07.05.17
 * Time: 17:48
 */

namespace Game\Unit;

use Game\Player\Player;
use Game\Unit\Exception\TypeNotExists;
use Game\Unit\Types\Base;

class UnitFactory{

    /**
     * Player object
     * @var Player
     */
    protected $player;

    /**
     * UnitFactory constructor.
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    public function createUnit(string $unitType, int $x, int $y)
    {
        if(!class_exists($unitType)){
            throw new TypeNotExists('Such type doesn\'t exist');
        }
        /** @var Base $unit */
        $unit = new $unitType($x, $y);
        $unit->setPlayerId($this->player->getPlayerId());
        return $unit;
    }



}